Hello guys, I've written this file to explain a little bit how does the project works.

1�- If the project it's not running it might be due the fact that the reference to TrailersOMDBWebAPI is not correct.
In order to solve this problem just right-click on the project solution references and add the TrailersOMDBWebAPI 
reference which is on the path ..\TrailersOMDBWebAPI\TrailersOMDBWebAPI\bin on the file TrailersOMDBWebAPI.dll

2�- When the project start's up my HomeController returns the index page that draws a yellow div with some information.
When you click on the search button (blue button), it redirects the page to the Search View that render the base
search engine page.

3�- When you are typing on the input field I have a jquery function that calls directly OMDBAPI via ajax call on each keyup.
The returned information from the API its treated and its showed like a smart search.

3.1 - If you click the movie that comes from the autocompleteEngine() function it calls the function called clicksearch() 
that gets the value of the title chosen and submits the form with that information.

3.2 - If you just type something on the input field and press enter it will submit the information as well.

4� - As soon as the information its submited  the (Post)Search method its triggered, starting to check if the information
that you've requested its on cache memory. If it is returns that information casted to a model called Movie.

5� - If the information its not in the cache memory the system starts to get from the web.config page the youtube Api key.
Then instantiate the Api controller and creates a Api Model through the function Get(MovieIn, youtubekey).

6� - The Get function present on the MainController from the project TrailersOMDBWebAPI creates two controllers, the
YoutubeController and the OmdbController which are responsible for creating their own model with the functions GetVideoID
and GetOMDBRating.

7�- The two functions (GetVideoID and GetOMDBRating) work the same way. They create a http client that
request the json information from the API's that i've used (OMDBAPI and YoutubeAPI).

8� - The returned information from the API its mapped into a Model Object to each controller (YoutubeController 
and OmdbController) and that object its return to the main controller.

9� - The main controller takes the two objects (YoutubeModel, OmdbModel) and create a new one with my own structure,
that is actually the recived object on the front end page.

10� - When the API Model its return to the front end page, the system create a new Model called Movie with the information
provided by the API Model

11� - The Movie its saved on the cache memory and returned to the Search View.

12� - On the Search View I've imported the Movie Model in order to use strongly typed information 
provided by the Movie Model.



 




