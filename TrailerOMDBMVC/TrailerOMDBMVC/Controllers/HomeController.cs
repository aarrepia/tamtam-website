﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using TrailerOMDBMVC.Models;

namespace TrailerOMDBMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }


        //GET home/search
        [HttpGet]
        public ActionResult Search()
        {
            var movie = new Movie();

            return View(movie);
        }


        //POST home/search/movietitle
        [HttpPost]
        public ActionResult Search(string MovieIn)
        {
            Movie trailerinfo;

            //Cached Items
            Cache cached = HttpRuntime.Cache;


            if (cached[MovieIn] == null)
            {
                //WebAPI call to get movie info and movie trailer
                string youtubekey = ConfigurationManager.AppSettings["YoutubeKey"];
                TrailersOMDBWebAPI.Controllers.MainController ApiController = new TrailersOMDBWebAPI.Controllers.MainController();
                TrailersOMDBWebAPI.Models.MainModel ApiModel = ApiController.Get(MovieIn, youtubekey);
                trailerinfo = new Movie
                {
                    Title = ApiModel.omdb.Title,
                    YoutubeID = ApiModel.youtube.items[0].id.videoId,
                    IMDBRating = ApiModel.omdb.imdbRating,
                    Poster = ApiModel.omdb.Poster,
                    Year = ApiModel.omdb.Year,
                    Type = ApiModel.omdb.Type,
                    Plot = ApiModel.omdb.Plot
                };

                //Add item to cache
                cached[MovieIn] = (Movie)trailerinfo;

            }
            return View((Movie)cached[MovieIn]);

        }
    }
}