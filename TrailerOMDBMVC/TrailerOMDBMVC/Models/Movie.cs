﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrailerOMDBMVC.Models
{
    public class Movie
    {
        public Movie() { }

        public string Title { get; set; }
        public string YoutubeID { get; set; }
        public string IMDBRating { get; set; }
        public string Poster { get; set; }
        public string Year { get; set; }
        public string Type { get; set; }
        public string Plot { get; set; }

        public bool Empty
        {
            get
            {
                return (string.IsNullOrWhiteSpace(Title) &&
                        string.IsNullOrWhiteSpace(YoutubeID)&&
                        string.IsNullOrWhiteSpace(Poster));
                }
        }

    }
}